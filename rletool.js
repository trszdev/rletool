//node.js
ESCAPE_CHAR = 33;

function _push_many(elem, count){
    for(var i=0;i<count;i++)
        this.push(elem);
}


function escape_encode(buffer){
    var temp = 1, result = [];
    result.push_many = _push_many;
    var last_char = buffer[0];
    var last_is_escape = last_char==ESCAPE_CHAR;
    function _flush(){
        if (temp>3 || last_is_escape){
            result.push(ESCAPE_CHAR);
            result.push(last_char);
            if (!last_is_escape) temp-=3;
            result.push(temp);
        } else result.push_many(last_char, temp);
        temp = 1;
    }
    for(var i=1;i<buffer.length;i++){
        var current_char = buffer[i];
        var temp_limit = last_is_escape?255:258;
        if (current_char!=last_char || temp==temp_limit)  {
            _flush();
            last_char = current_char;
            last_is_escape = last_char==ESCAPE_CHAR;
        }
        else temp++;
    }
    _flush();
    return Buffer(result);
}


function escape_decode(buffer){
    var result = [];
    result.push_many = _push_many;
    for(var i=0;i<buffer.length;i++){
        var current_char = buffer[i];
        if (current_char==ESCAPE_CHAR){
            var repeated_char = buffer[++i];
            var count = buffer[++i];
            if (repeated_char!=ESCAPE_CHAR) count+=3;
            result.push_many(repeated_char, count);
        }
        else result.push(current_char);
    }
    return Buffer(result);
}


function main(args) {
    if (['encode', 'decode'].indexOf(args[1])==-1 || args.length != 4) {
        console.log('\tusage: node rletool.js [encode|decode] [infile] [outfile]');
        return 1;
    }
    const fs = require('fs');
    var buffer = fs.readFileSync(args[2]);
    var changed_buffer = args[1]=='encode' ? escape_encode(buffer) : escape_decode(buffer);
    fs.writeFileSync(args[3], changed_buffer);
    return 0;
}

process.exit(main(process.argv.slice(1)));
