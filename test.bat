@echo off
set RLETOOL=node rletool.js
set TESTFILES=(example.jpg, example.msg, exotic.msg)

:: shorten this
for %%i in %TESTFILES% do %RLETOOL% encode %%i %%i.encoded
for %%i in %TESTFILES% do %RLETOOL% decode %%i.encoded %%i.decoded
for %%i in %TESTFILES% do fc /b %%i.decoded %%i
pause